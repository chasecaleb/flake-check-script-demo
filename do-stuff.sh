#!/usr/bin/env bash

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# Resolves to directory of this script, so that it can find files relative to itself regardless of
# where it's called from.
script_dir=${BASH_SOURCE%/*}
destination=$1

# since source is nix store which is read-only.
cp --no-target-directory --recursive "$script_dir/../foo" "$destination"
chmod --recursive u+w "$destination"

cd "$destination"
sed -i 's/two/SED DID STUFF/' some-data
echo "First script done"
