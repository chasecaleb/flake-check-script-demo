{
  description = "Demo of pain with nix flake check for shell scripts";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
    rec
    {
      packages.${system} = {
        scripts = pkgs.resholve.mkDerivation {
          pname = "scripts";
          version = "unset";
          src = ./.;
          installPhase = ''
            install -Dv do-stuff.sh $out/bin/do-stuff.sh
            install -Dv test.sh $out/bin/test.sh
            find ./foo -type f -exec install -Dvm 644 "{}" "$out/{}" \;
          '';
          solutions = {
            default = {
              scripts = [ "bin/do-stuff.sh" "bin/test.sh" ];
              interpreter = "${pkgs.bashInteractive}/bin/bash";
              # NOTE THE MAGIC HERE: test.sh calls do-stuff.sh, so it needs to be an input too.
              inputs = [ "bin/do-stuff.sh" pkgs.coreutils pkgs.gnugrep pkgs.gnused ];
              # Also, tell resholve that do-stuff.sh doesn't exec it's args
              execer = [ "cannot:bin/do-stuff.sh" ];
            };
          };
        };
      };

      checks.${system}.check-test = pkgs.runCommand "check-test" { } ''
        ${packages.${system}.scripts}/bin/test.sh | tee $out
      '';
    };
}
