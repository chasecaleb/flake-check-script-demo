#!/usr/bin/env bash

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

# realpath necessary due to cd change
tmp_dir=$(mktemp -d)
bin/do-stuff.sh "$tmp_dir"

grep -q "SED DID STUFF" "$tmp_dir/some-data"
echo "Success:"
cat "$tmp_dir/some-data"
